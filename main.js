import { act1, act2, act3, act4, act5, act6, act7, act8 } from './exercise/exe1.js'
// var boucles = [];

// for (var i = 0; i < 10; i++) {
//     boucles.push(i);
// }

// console.log(boucles)

console.log("Exercice 1");
console.log(act1());

console.log("Exercice 2");
console.log(act2());


console.log("Exercice 3");
console.log(act3());

console.log("Exercice 4");
console.log(act4());

console.log("Exercice 5");
console.log(act5());

console.log("Exercice 6");
console.log(act6());

console.log("Exercice 7");
console.log(act7());

console.log("Exercice 8");
console.log(act8());

import { hello, act1_2, act2_3, act2_4, act2_5, act2_6, act2_7, act2_8 } from './exercise/exe2.js'

console.log("Exercice 1");
console.log(hello());

console.log("Exercice 2");
console.log(act1_2("hello"));

console.log("Exercice 3");
console.log(act2_3())

console.log("Exercice 4");
console.log(act2_4(6, 7))


console.log("Exercice 5");
console.log(act2_5())


console.log("Exercice 6");
console.log(act2_6())

console.log("Exercice 7");
console.log(act2_7('femme', 20))

console.log("Exercice 8");
console.log(act2_8())


import { act3_1, act3_2, act3_3, act3_4, act3_5, act3_6, act3_7, act3_8, act3_9, act3_10 } from './exercise/exe3.js'

console.log("Exercice 1");
console.log(act3_1())


console.log("Exercice 2");
console.log(act3_2())

console.log("Exercice 3");
console.log(act3_3())

console.log("Exercice 4");
console.log(act3_4())

console.log("Exercice 5");
console.log(act3_5())

console.log("Exercice 6");
console.log(act3_6())

console.log("Exercice 7");
console.log(act3_7())

console.log("Exercice 8");
console.log(act3_8())

console.log("Exercice 9");
console.log(act3_9())

console.log("Exercice 10");
console.log(act3_10())